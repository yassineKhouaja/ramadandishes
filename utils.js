import axios from 'axios';
import fs from 'fs';
import path from 'path';
import { fileURLToPath } from 'url';

export const __filename = fileURLToPath(import.meta.url);
export const __dirname = path.dirname(__filename);
export const dishes = JSON.parse(
    fs.readFileSync(`${__dirname}/data/dishes.json`)
);

export const convertToDate = (apiTime) => {
    const time = apiTime.split(' ')[0].split(':');
    return new Date(`2022/03/29 ${time[0]}:${time[1]}`);
};

export const calculateTime = (asr, maghrib) => {
    const asrDate = convertToDate(asr);
    const maghribDate = convertToDate(maghrib);

    return (maghribDate - asrDate) / (1000 * 60);
};

export const formatDish = (dish, timeBetweenAsrMaghrib) => {
    const remaningTime = timeBetweenAsrMaghrib - dish.duration - 15;
    dish.cooktime =
        remaningTime > 0
            ? `${remaningTime} minutes after Asr`
            : `${-remaningTime} minutes before Asr`;
    delete dish.duration;
    return dish;
};

export const fetchData = async () => {
    const {
        data: { data },
    } = await axios(
        `http://api.aladhan.com/v1/hijriCalendar?latitude=35.821430&longitude=10.634422&method=1&month=9&year=1437`
    );
    return data;
};
