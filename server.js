import dotenv from 'dotenv';
import express from 'express';
import morgan from 'morgan';
import 'express-async-errors';
// routers
import dishesRoutes from './routes/dishesRoutes.js';
// middleware
import errorHandlerMiddleware from './middleware/error-handler.js';
import notFoundMiddleware from './middleware/not-found.js';
dotenv.config();

const app = express();

if (process.env.NODE_ENV !== 'production') {
    app.use(morgan('dev'));
}
app.use(express.json());

app.use('/', dishesRoutes);

app.use(notFoundMiddleware);

app.use(errorHandlerMiddleware);

const port = process.env.PORT || 3000;

app.listen(port);
