import express from 'express';

import {
    getAllDishes,
    getRandomDish,
} from '../controllers/dishesController.js';

const router = express.Router();

router.route('/cooktime').get(getAllDishes);
router.route('/suggest').get(getRandomDish);

export default router;
