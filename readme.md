<p align="center">
  <a href="https://takiacademy.com">
  <img src="https://scontent.ftun2-1.fna.fbcdn.net/v/t1.18169-1/20525703_1830196756996515_5562753936043243990_n.png?stp=dst-png_p148x148&_nc_cat=103&ccb=1-5&_nc_sid=1eb0c7&_nc_ohc=EhlcknCa5c0AX8Kn3g5&_nc_ht=scontent.ftun2-1.fna&oh=00_AT9Eipv-j7zqAILBDz_Sv7i57MlKXwMkCW0EQCHoiKIVwg&oe=626A94C9">
  </a>
</p>
<p align="center">

  <br>
  <a href="https://www.linkedin.com/in/yassine-khouaja-a9707253">Yassine Khouaja</a> | <a href="https://gitlab.com/yassineKhouaja/ramadandishes">Gitlab Repo</a> | <a a href = "mailto: yassinekhouaja@gmail.com">E-mail</a>
  <br>

# Ramadan Dishes Challenge

-   This a NodeJS api using the framework express.
-   The app serve on port `3000`, thus listening to the following URL: `http://localhost:3000`

  <img src="https://media1.thehungryjpeg.com/thumbs2/ori_3756349_ubv55r7dc1njlqok3grp3un2a4np0izdmqc4opju_islam-people-praying-illustration.jpg">
  <br>
  <br>
</p>

# Run the project

```shell
git clone https://gitlab.com/yassineKhouaja/ramadandishes.git ramadandishes
```

```shell
cd ramadandishes/
```

```shell
npm install
```

```shell
npm start
```

## Endpoint 1: Cooking time

-   The endpoint take two query params, which are:
    -   `ingredient`: The required ingredient in the dish.
    -   `day`: The _Ramadan_ day in which the dish will be cooked

```js
// GET http://localhost:3000/cooktime?ingredient=Meat&day=27
// Response:
[
    {
        name: 'BBQ',
        ingredients: ['Meat', 'Harissa', 'Black Pepper', 'Garlic'],
        cooktime: '11 minutes after Asr',
    },
    {
        name: 'Kamuniya',
        ingredients: ['Meat', 'Cumin', 'Tomatoe Paste'],
        cooktime: '103 minutes after Asr',
    },
    {
        name: 'Dweeda',
        ingredients: [
            'Dweeda',
            'Tomatoe Paste',
            'Meat',
            'Potatoe',
            'Carrot',
            'Onion',
        ],
        cooktime: '119 minutes before Asr',
    },
    {
        name: 'Mermez',
        ingredients: ['Chickpea', 'Tomatoe Paste', 'Meat', 'Onion'],
        cooktime: '81 minutes after Asr',
    },
];
```

## Endpoint 2: Suggestions

-   The endpoint take one query param:
    -   `day`: The _Ramadan_ day in which the dish will be cooked
-   The endpoint respond with one dish with the same format as the previous endpoint.

```js
// GET http://localhost:3000/suggest?day=25
// Response:
{
    "name": "Cannelloni",
    "ingredients": [
        "Chicken",
        "Cannelloni",
        "Onion",
        "Tomatoe Paste"
    ],
    "cooktime": "42 minutes after Asr"
}
```

## The packages used in the app

[express](https://www.npmjs.com/package/express) : Fast, unopinionated, minimalist web framework for node.<br>
[axios](https://www.npmjs.com/package/axios) : Promise based HTTP client for the browser and node.js<br>
[dotenv](https://www.npmjs.com/package/dotenv) : Loads environment variables from .env file.<br>
[express-async-errors](https://www.npmjs.com/package/express-async-errors) : Async/await error handling support for expressjs. <br>
[http-status-codes](https://www.npmjs.com/package/http-status-codes): onstants enumerating the HTTP status codes. <br>
[morgan](https://www.npmjs.com/package/morgan): HTTP request logger middleware for node.js. <br>
