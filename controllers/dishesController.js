import { StatusCodes } from 'http-status-codes';
import { BadRequestError } from '../errors/index.js';
import { calculateTime, dishes, fetchData, formatDish } from '../utils.js';

export const getAllDishes = async (req, res) => {
    const { ingredient: ingredientReq, day } = req.query;
    if (typeof +day !== 'number' || day > 30) {
        throw new BadRequestError('please provide a valid date number');
    }
    if (!ingredientReq) {
        throw new BadRequestError('please provide an ingredient');
    }
    const ingredient =
        ingredientReq.charAt(0).toUpperCase() +
        ingredientReq.slice(1).toLowerCase();
    let data;
    try {
        data = await fetchData();
    } catch (error) {
        throw new BadRequestError(error.message || 'something went wrong');
    }
    const { Asr, Maghrib } = data[Number(day) - 1].timings;
    const timeBetweenAsrMaghrib = calculateTime(Asr, Maghrib);
    const dishesRes = JSON.parse(JSON.stringify(dishes))
        .filter((dish) => dish.ingredients.includes(ingredient))
        .map((resDish) => formatDish(resDish, timeBetweenAsrMaghrib));
    res.status(StatusCodes.OK).json(dishesRes);
};

export const getRandomDish = async (req, res) => {
    const { day } = req.query;
    if (typeof +day !== 'number' || day > 30) {
        throw new BadRequestError('please provide a valid date number');
    }
    const randomDish = Math.floor(Math.random() * dishes.length);
    const data = await fetchData();
    const { Asr, Maghrib } = data[Number(day) - 1].timings;
    const timeBetweenAsrMaghrib = calculateTime(Asr, Maghrib);
    const dish = JSON.parse(JSON.stringify(dishes[randomDish]));
    const resDish = formatDish(dish, timeBetweenAsrMaghrib);
    res.status(StatusCodes.OK).json(resDish);
};
